package net.stawrul.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

/**
 * Klasa encyjna reprezentująca towar w sklepie (książkę).
 */
@Entity
@EqualsAndHashCode(of = "id")
@NamedQueries(value = {
        @NamedQuery(name = Book.FIND_ALL, query = "SELECT b FROM Book b"),
        @NamedQuery(name=Book.FIND_BY_MAX_PRICE, query="SELECT b FROM Book b WHERE PRICE<")
})
public class Book {
    public static final String FIND_ALL = "Book.FIND_ALL";
    public static final String FIND_BY_MAX_PRICE="Book.FIND_MAX_PRICE";
    @Getter
    @Id
    UUID id = UUID.randomUUID();

    @Getter
    @Setter
    String title;
    
    @Getter
    @Setter
    Integer amount;
    
    @Getter
    @Setter
    String publisher;
    
    @Getter
    @Setter
    Double price;
    
    @Getter
    @Setter
    Integer pages;

    
}
